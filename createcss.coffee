window.createCSS = ( selector, style ) ->
  return if not document.styleSheets

  return if document.getElementsByTagName("head").length == 0

  if document.styleSheets.length
    for sheet in document.styleSheets
      if sheet.disabled then continue

      media     = sheet.media
      mediaType = typeof sheet.media

      if mediaType == "string" and (sheet.media == "" or sheet.media.indexOf("screen") != -1)
        styleSheet = sheet
        break

      if mediaType == "object" and (sheet.media == "" or sheet.media.indexOf("screen") != -1)
        styleSheet = sheet
        break

  if not styleSheet?
    styleSheetElement = document.createElement("style")

    styleSheetElement.type = "text/css"

    document.getElementsByTagName("head")[0].appendChild styleSheetElement ;

    for sheet in document.styleSheets
      if sheet.disabled
        continue

      styleSheet = sheet

    media = styleSheet.media
    mediaType = typeof media

  if mediaType == "string"
    for rule in styleSheet.rules
      if rule.selectorText? and sheet.selectorText.toLowerCase() == selector.toLowerCase()
        rule.style.cssText = style
        return

    styleSheet.addRule(selector, style)
  else if mediaType == "object"
    for rule in styleSheet.cssRules
      if rule.selectorText? and rule.selectorText.toLowerCase() == selector.toLowerCase()
        rule.style.cssText = style
        return

    styleSheet.insertRule selector + "{" + style + "}", styleSheet.cssRules.length

window.updateDoc = ( element ) ->

  css = window.document.getElementById(element).value.replace( /[\n\r\t ]+/g, " " );

  console.log css

  while css
    try
      objectName = css.slice 0, css.search "{"

      restCSS = css.slice objectName.length+1, css.search("}") - 1;
    catch e
      return

    console.log objectName + restCSS

    return false if ! objectName? or ! restCSS?

    console.log "Name: #{objectName}"
    console.log "    : #{restCSS}"

    createCSS objectName, restCSS

    css = css.slice css.search("}")+1, css.length
