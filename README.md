Coffeescript-created Javascript-added Stylesheets
=================================================

A small bit of coffeescript that demonstrates how JS/CS can be used to dynamically add/update stylesheets on a webpage.  
This can be used in a wide range of applications, from completely dynamic document animation to WYSIWYG HTML in-browser 
editors.  

Free for any use whatsoever, adapted from this code on stack overflow: 

http://stackoverflow.com/questions/1720320/how-to-dynamically-create-css-class-in-javascript-and-apply
